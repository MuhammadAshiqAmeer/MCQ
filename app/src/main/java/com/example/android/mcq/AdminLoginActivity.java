package com.example.android.mcq;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AdminLoginActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText adminEmail;
    private EditText adminPassword;
    private ProgressDialog loadingbar;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        loginButton = findViewById(R.id.loginAdminButton);
        adminEmail = findViewById(R.id.adminEmail);
        adminPassword = findViewById(R.id.adminPassword);

        firebaseAuth = FirebaseAuth.getInstance();
        loadingbar = new ProgressDialog(this);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allowAdminLogin();
            }
        });
    }

    private void allowAdminLogin() {

        if (firebaseAuth.getCurrentUser() != null) {
            Intent intent = new Intent(AdminLoginActivity.this, SubjectActivity.class);
            startActivity(intent);
            finish();
        } else {
            String email = adminEmail.getText().toString();
            String password = adminPassword.getText().toString();
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Please enter email...", Toast.LENGTH_SHORT).show();
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Please enter password...", Toast.LENGTH_SHORT).show();
            } else {

                loadingbar.setTitle("Sign in");
                loadingbar.setMessage("please wait...");
                loadingbar.setCanceledOnTouchOutside(true);
                loadingbar.show();

                firebaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    Toast.makeText(AdminLoginActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                                    loadingbar.dismiss();

                                    Intent intent = new Intent(AdminLoginActivity.this, SubjectActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    String message = task.getException().toString();
                                    Toast.makeText(AdminLoginActivity.this, "Error:" + message, Toast.LENGTH_SHORT).show();
                                    loadingbar.dismiss();
                                }
                            }
                        });

            }

        }

    }
}
