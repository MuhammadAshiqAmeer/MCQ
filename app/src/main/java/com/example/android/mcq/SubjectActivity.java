package com.example.android.mcq;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubjectActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Button addSubButton;
    private TextView signOut;
    private FirebaseAuth firebaseAuth;
    private ImageView delete;
    private  String newSub;
    private static FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        delete = findViewById(R.id.subDeleteButton);
        recyclerView = findViewById(R.id.recyclerView);
        addSubButton = findViewById(R.id.addSubject);
        signOut = findViewById(R.id.signOut);

        db=FirebaseFirestore.getInstance();

        firebaseAuth = FirebaseAuth.getInstance();
        List<String> list=new ArrayList<>();
        list.add("Sub 1");
        list.add("Sub 2");
        list.add("Sub 3");
        list.add("Sub 4");
        list.add("Sub 5");

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        SubjectAdapter adapter = new  SubjectAdapter(list);
        recyclerView.setAdapter(adapter);
        checkAdminLogged();

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                Intent intent =new Intent(SubjectActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        addSubButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSubject();
            }
        });
    }

    private void addSubject() {
        LayoutInflater inflater = LayoutInflater.from(SubjectActivity.this);
        final View yourCustomView = inflater.inflate(R.layout.add_subject, null);

        final TextView etName = (EditText) yourCustomView.findViewById(R.id.subname);
        AlertDialog dialog = new AlertDialog.Builder(SubjectActivity.this)
                .setTitle("Enter subject name")
                .setView(yourCustomView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Map<String, Object> subject = new HashMap<>();
                        subject.put("ID", etName.getText().toString());
                        db.collection("Subjects")
                                .add(subject)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Toast.makeText(SubjectActivity.this, "Subject added with id: " + documentReference.getId(), Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(SubjectActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                                    }
                                });

                    }
                })
                .setNegativeButton("Cancel", null).create();
        dialog.show();


    }

    private void checkAdminLogged() {
        if(firebaseAuth.getCurrentUser()==null){
            addSubButton.setVisibility(View.GONE);
            signOut.setVisibility(View.GONE);
        }
    }


}
