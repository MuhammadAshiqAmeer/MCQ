package com.example.android.mcq;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ViewHolder> {


    private List<String> subjectList;
    private FirebaseAuth firebaseAuth;

    public SubjectAdapter(List<String> subjectList) {
        this.subjectList = subjectList;
        firebaseAuth=FirebaseAuth.getInstance();

    }

    @NonNull
    @Override
    public SubjectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subjects_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectAdapter.ViewHolder viewHolder, int pos) {
        String title = subjectList.get(pos);
        viewHolder.setData(title);
    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView subName;
        private ImageView delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subName = itemView.findViewById(R.id.subjectName);
            delete = itemView.findViewById(R.id.subDeleteButton);
            if(firebaseAuth.getCurrentUser()==null){
                delete.setVisibility(View.GONE);

            }

        }

        private void setData(String title){
            subName.setText(title);

        }
    }
}
