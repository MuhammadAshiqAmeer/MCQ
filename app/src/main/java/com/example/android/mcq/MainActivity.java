package com.example.android.mcq;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog loadingbar;
    private EditText name, registerNo;
    private Button registerButton;
    private TextView adminLogin;


    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private static FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        loadingbar=new ProgressDialog(this);

        name = findViewById(R.id.name);
        registerNo = findViewById(R.id.registerNo);
        registerButton = findViewById(R.id.RegisterButton);
        adminLogin = findViewById(R.id.adminLoginText);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        db = FirebaseFirestore.getInstance();




        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                // Create a new user with a first and last name
                addUserData();
            }
        });

        adminLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i3 = new Intent(MainActivity.this, AdminLoginActivity.class);
                MainActivity.this.startActivity(i3);
                finish();
            }
        });


    }

    private void addUserData() {
        Map<String, Object> user = new HashMap<>();
        user.put("name",name.getText().toString());
        user.put("regNo",registerNo.getText().toString());
        user.put("score", "0");
        db.collection("users")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(MainActivity.this, "Registered", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(MainActivity.this,SubjectActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( Exception e) {
                        Toast.makeText(MainActivity.this, "Error adding data", Toast.LENGTH_SHORT).show();
                    }
                });

    }


}




